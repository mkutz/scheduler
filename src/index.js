import { Task } from 'genetic';
import options from './options.js';
import crossover from './crossover.js';
import getRandomSolution from './getRandomSolution.js';
import stagnating from './stopCriteria.js';
import fitness from './fitness.js';
import mutate from './mutate.js';

let taskInstance = new Task({
    getRandomSolution : getRandomSolution,
    popSize : options.popSize,
    stopCriteria : stagnating(options.threshold),
    fitness : fitness,
    minimize : options.minimize,
    mutateProbability : options.mutateProbability,
    mutate : mutate,
    crossoverProbability : options.crossoverProbability,
    crossover : crossover,
});

console.log("start");
taskInstance.run( stats => {
    console.log('~drum roll~ results: ', JSON.stringify(stats.max) + ";;;;;;" + stats.maxScore);
    debugger;
} )

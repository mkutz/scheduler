module.exports = {
    "threshold": 0,
    "popSize" : 5,
    "minimize": false,
    "mutateProbability": 0.1,
    "crossoverProbability": 0.3,
}
